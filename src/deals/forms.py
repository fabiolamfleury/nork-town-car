from flask_wtf import FlaskForm
from deals.enums import VehicleColorEnum, VehicleModelEnum
from wtforms import StringField, SelectField, EmailField
from wtforms.validators import DataRequired


class CitizenForm(FlaskForm):
    name = StringField("name", validators=[DataRequired()])
    email = EmailField("email", validators=[DataRequired()])
    security_number = StringField("security number", validators=[DataRequired()])


class VehicleSaleForm(FlaskForm):
    ownerd_id = SelectField("Owner", coerce=int)
    vehicle_model = SelectField(
        "Vehicle model",
        choices=VehicleModelEnum.choices(),
        coerce=VehicleModelEnum.coerce,
    )
    vehicle_color = SelectField(
        "Vehicle color",
        choices=VehicleColorEnum.choices(),
        coerce=VehicleModelEnum.coerce,
    )
