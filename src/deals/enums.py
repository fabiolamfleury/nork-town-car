import enum


class FormEnum(enum.Enum):
    @classmethod
    def choices(cls):
        return [(choice, choice.name) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return str(item) if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.value)


class VehicleColorEnum(FormEnum):
    yellow = "yellow"
    blue = "blue"
    gray = "gray"


class VehicleModelEnum(FormEnum):
    hatch = "hatch"
    sedan = "sedan"
    conversible = "conversible"
