import os
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import func, select, event
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.hybrid import hybrid_method
from config import db
from .enums import VehicleColorEnum, VehicleModelEnum


MAX_VEHICLES_PER_CITIZEN = 3


class Citizen(db.Model):
    __tablename__ = "citizen"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    security_number = db.Column(db.String(128), unique=True, nullable=False)
    owned_vehicles = relationship("Vehicle")

    def __init__(self, email):
        self.email = email

    @hybrid_method
    def sale_opportunity(self, point):
        return self.owned_vehicles.count == 0

    @staticmethod
    def get_possible_clients():
        engine = db.create_engine(
            os.getenv("DATABASE_URL", "sqlite://"), engine_opts={}
        )
        Session = sessionmaker(bind=engine)
        session = Session()
        subsearch = (
            select([func.count(Citizen.owned_vehicles)])
            .where(Citizen.id == Vehicle.owner)
            .as_scalar()
        )
        return session.query(Citizen).filter(subsearch < 3).all()


class Vehicle(db.Model):
    __tablename__ = "vehicle"

    id = db.Column(db.Integer, primary_key=True)
    vehicle_color = db.Column(db.Enum(VehicleColorEnum), nullable=False)
    vehicle_model = db.Column(db.Enum(VehicleModelEnum), nullable=False)
    owner = db.Column(db.Integer, db.ForeignKey("citizen.id"))


@event.listens_for(Vehicle.owner, "set", retval=True)
def vehicle_per_citizen_check(target, value, oldvalue, initiator):
    if value is not None:
        vehicle_count = Vehicle.query.filter_by(citizen_id=value).count()
        if vehicle_count >= MAX_VEHICLES_PER_CITIZEN:
            orig = Exception(
                "Maximum number of Vehicle ({}) "
                "reached for Citizen.id = {}".format(MAX_VEHICLES_PER_CITIZEN, value)
            )
            msg = "Record Not Committed"
            raise IntegrityError(msg, ";)", orig)
    return value
