import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite://")
    SQLALCHEMY_TRACK_MODIFICATIONS = False


SECRET_KEY = os.urandom(32)
app = Flask(__name__)
app.config.from_object(Config)
app.config["SECRET_KEY"] = SECRET_KEY

db = SQLAlchemy(app)
router = app.route

import routes
