from flask import render_template, redirect, url_for, flash
from config import router, db
from deals.forms import VehicleSaleForm, CitizenForm
from deals.models import Citizen, Vehicle


@router("/citizens/new", methods=["GET", "POST"])
def new_citizen():
    citizen_form = CitizenForm()

    if citizen_form.validate_on_submit():
        citizen = Citizen(
            citizen_form.name.data,
            citizen_form.email.data,
            citizen_form.security_number.data
        )
        db.session.add(citizen)
        db.session.commit()
        return redirect(url_for("register_sale"))

    context = {"form": citizen_form}
    return render_template("new_citizen.jinja2", **context)


@router("/sale", methods=["GET", "POST"])
def register_sale():
    vehicle_form = VehicleSaleForm()

    if vehicle_form.validate_on_submit():
        vehicle = Vehicle(
            vehicle_form.vehicle_color.data,
            vehicle_form.vehicle_model.data,
            vehicle_form.ownerd_id.data
        )
        db.session.add(vehicle)
        db.session.commit()
        return redirect(url_for("list_citizens"))
    else:
        flash(vehicle_form.errors.items())

    citizens_available = Citizen.get_possible_clients()

    context = {"form": vehicle_form, "citizens_available": citizens_available}
    return render_template("register_sale.jinja2", **context)


@router("/citizens", methods=["GET"])
def list_citizens():
    pass
